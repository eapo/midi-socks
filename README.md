# midi-socks

quick sketch to use a smartphone as a midi controller over websockets

currently only has basic faders, toggle/momentary pads and x-y matrices.

written in coffeescript, using socket.io, easy-midi, p5.js

## install 

get [node.js](https://nodejs.org/)

`git clone https://gitlab.com/unlessgames/midi-socks.git`

`cd midi-socks`

`npm install`

## usage

your host and control device needs to be on the same network, then

`npm start`

once the server is running, open a browser on your phone with the address you got in the console

the "?" icon shows you channels of each module and the individual controller numbers, it also puts the browser into full-screen on phones

the node process creates a virtual midi device and sends cc through that

tested on linux


## customizing

edit the **rack** list at the end of either *client.js* or *client.coffee* (if you have coffee compiler) 

defaults are the following

```
rack = [
  pads4x4(0),
  pads4x4momentary(1),
  sliders(2),
  sliders(2, 8),
  xypad(3),
  xypad(3, 3)
]
```

the first argument sets which MIDI *channel* should the module use to send the messages

the second, optional argument is an *offset* for controller number mapping, by default a module will use controller numbers from zero to as many controls it has. 
the 2 sliders are now filling up 0-15 on channel 2 thanks to the 8 offset on the second one

for example you could have 5 xypads on channel 1 (they have each 3 controls : *x*, *y* and *press*, which is an on/off signal to show if you are currently touching the screen or not)

```
rack = [
  xypad(0),
  xypad(0, 3),
  xypad(0, 6),
  xypad(0, 9),
  xypad(0, 12)
]
```

channels 1 is 0 in code
